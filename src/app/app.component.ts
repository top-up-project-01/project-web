import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { allFaculties } from './graphql/faculty';
import { getRecentPublishedTopics } from './graphql/topic'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'webAPI';
  facultyId = localStorage.getItem("facultyId");



  constructor(private apollo: Apollo) { }
  ngOnInit(): void {
    this.apollo
      .watchQuery({
        query: getRecentPublishedTopics,
        variables: {
          facultyId: this.facultyId
        }
      })
      .valueChanges.subscribe((result: any) => {
        console.log(result.data.getRecentPublishedTopics)
        console.log(this.facultyId)
      });


  }




}
