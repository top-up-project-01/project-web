import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponentComponent } from './components/Login-component/Login-component.component';
import { MaterialModule } from './material/material.module';
import { Menu_barComponentComponent } from './components/Menu_bar-component/Menu_bar-component.component';
import { HomePage_componentComponent } from "./components/HomePage_component/HomePage_component.component";
import { IvyCarouselModule } from "angular-responsive-carousel";
import { FooterComponentComponent } from './components/footer-component/footer-component.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { SearchComponentComponent } from "./components/search-component/search-component.component";
import { APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { InMemoryCache } from '@apollo/client/core';
import { FormsModule } from '@angular/forms';
import { IndexPage_ComponentComponent } from "./components/indexPage_Component/indexPage_Component.component";
import { SidebarModule } from 'ng-sidebar';
import { StudentPage_ComponentComponent } from "./components/StudentPage_Component/StudentPage_Component.component";
import { AngularFileUploaderModule } from 'angular-file-uploader';
import { NgImageSliderModule } from 'ng-image-slider';
import { Upload_pageComponent } from './components/upload_page/upload_page.component';
import { SearchDashboardPageComponent } from './components/searchDashboardPage/searchDashboardPage.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { Topic_componentComponent } from './components/topic_component/topic_component.component';
import { RecenTopicComponent } from './components/recenTopic/recenTopic.component';
import { Topic_listComponent } from './components/topic_list/topic_list.component';
import { ContactUsComponent } from './components/contactUs/contactUs.component';
import { SubmissionDetailComponent } from './components/submissionDetail/submissionDetail.component';
@NgModule({
  declarations: [
    AppComponent,
    SubmissionDetailComponent,
    ContactUsComponent,
    RecenTopicComponent,
    SearchDashboardPageComponent,
    LoginComponentComponent,
    Menu_barComponentComponent,
    HomePage_componentComponent,
    FooterComponentComponent,
    SearchComponentComponent,
    Topic_componentComponent,
    IndexPage_ComponentComponent,
    StudentPage_ComponentComponent,
    Upload_pageComponent,
    DashboardComponent,
    Topic_listComponent


  ],
  imports: [
    BrowserModule,
    NgImageSliderModule,
    AppRoutingModule,
    MaterialModule,
    IvyCarouselModule, GraphQLModule, HttpClientModule,
    FormsModule,
    SidebarModule.forRoot(),
    AngularFileUploaderModule,
  ],
  providers: [GraphQLModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
