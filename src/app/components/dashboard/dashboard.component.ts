import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { searchTopicsByNameAndCode } from '../../graphql/topic';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  token: any;
  role: any;
  fullName: any;
  inputCode: any;
  dataResult: any;
  email = localStorage.getItem('email');
  constructor(private apollo: Apollo) { }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.role = localStorage.getItem('role');
    this.fullName = localStorage.getItem('fullName');
  }
  search() {
    const topic_code = this.inputCode
    window.open("http://localhost:4200/index/dashboard/search?input=" + topic_code, "_self");
    console.log('search')
    // this.apollo.watchQuery({
    //   query: searchTopicsByNameAndCode,
    //   variables: {
    //     searchString: this.inputCode
    //   }
    // })
    //   .valueChanges.subscribe((result: any) => {
    //     const data = result.data.searchTopicsByNameAndCode
    //     this.dataResult = data;
    //     console.log(this.dataResult)
    //   });
  }
}
