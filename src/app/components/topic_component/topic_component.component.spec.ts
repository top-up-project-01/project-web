/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Topic_componentComponent } from './topic_component.component';

describe('Topic_componentComponent', () => {
  let component: Topic_componentComponent;
  let fixture: ComponentFixture<Topic_componentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Topic_componentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Topic_componentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
