import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { topicData } from "../../models/topic";
import { Apollo } from 'apollo-angular';
import { searchAllPublishedTopics } from 'src/app/graphql/topic';
import { getApprovedSubmission, getSubmissionsOfTopic, getApprovedSubmissionOfPublishedTopic } from 'src/app/graphql/submission';
@Component({
  selector: 'app-topic_component',
  templateUrl: './topic_component.component.html',
  styleUrls: ['./topic_component.component.scss']
})
export class Topic_componentComponent implements OnInit {
  topicData: any[] = topicData;
  constructor(private route: ActivatedRoute, private apollo: Apollo) { }



  isSecondDeadline?: boolean;
  topicCode?: string;
  topicId?: string;
  topicName?: string;
  subId: any;
  firstDeadline: any;
  secondDeadline: any;
  publishDate: any;
  today = new Date();
  description: any;
  status: any;


  // Thông tin user
  userId = localStorage.getItem('userId');
  role = localStorage.getItem('role');
  fullName = localStorage.getItem('fullName');
  facultyId = localStorage.getItem('facultyId');

  getData1: any;
  getData2: any[] = [];
  dataTopic: any[] = [];
  dataSubmisstion: any[] = [];
  listSubmisstion: any;
  result: any;


  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.topicCode = params['id']
    })
    this.apollo.watchQuery({
      query: searchAllPublishedTopics,
      variables: {
        searchString: this.topicCode,
      }
    })
      .valueChanges.subscribe((result: any) => {
        this.dataTopic = result.data.searchAllPublishedTopics
        this.result = this.dataTopic.find(element => element.topicCode === this.topicCode)
        this.firstDeadline = this.result['firstDeadline']
        this.secondDeadline = this.result['secondDeadline']
        this.publishDate = this.result['publishDate']
        this.description = this.result['description']
        this.topicId = this.result['id']
        this.topicName = this.result['name']
        console.log("topicName : " + this.topicName)
        console.log("user ID: " + this.userId)
        console.log("topic ID: " + this.topicId)
        console.log("firstDeadline: " + this.firstDeadline)
        console.log("secondDeadline: " + this.secondDeadline)
        console.log("publishDate: " + this.publishDate)

        this.apollo.watchQuery({
          query: getApprovedSubmissionOfPublishedTopic,
          variables: {
            facultyId: this.facultyId,
            topicId: this.topicId,
          }
        })
          .valueChanges.subscribe((result: any) => {
            this.dataSubmisstion = result.data.getApprovedSubmissionOfPublishedTopic;
            console.log(this.dataSubmisstion);
            this.listSubmisstion = this.dataSubmisstion.map(({ student }) => student);
          })
      });
  }
  getDate(value: string) {
    var date = new Date(value);
    return date
  }
  openSubmission(subId: string) {
    window.open("http://localhost:4200/index/submissionDetail?id=" + this.topicCode + "&subId=" + subId, "_self");
  }
}
