import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Apollo } from 'apollo-angular';
import { searchAllPublishedTopics } from 'src/app/graphql/topic';



@Component({
  selector: 'app-search-component',
  templateUrl: './search-component.component.html',
  styleUrls: ['./search-component.component.css']
})
export class SearchComponentComponent implements OnInit {

  constructor(private route: ActivatedRoute, private apollo: Apollo) { }
  code: any;

  result: any;

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.code = params['input'];
    })


    this.apollo.watchQuery({
      query: searchAllPublishedTopics,
      variables: {
        searchString: this.code
      }
    })
      .valueChanges.subscribe((result: any) => {
        const data = result.data.searchAllPublishedTopics
        this.result = data;
        console.log(this.result)
      });

  }
  getDate(value: string) {
    var date = new Date(value);
    return date
  }
  openTopic(topic_code: string) {
    window.open("http://localhost:4200/index/topic?id=" + topic_code, "_self");
  }
}