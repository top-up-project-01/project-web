import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Topic from "../../models/topic";
import { Apollo } from 'apollo-angular';
import { getRecentPublishedTopics, searchTopicsByNameAndCode, searchAllPublishedTopics } from 'src/app/graphql/topic';


@Component({
  selector: 'app-searchDashboardPage',
  templateUrl: './searchDashboardPage.component.html',
  styleUrls: ['./searchDashboardPage.component.scss']
})
export class SearchDashboardPageComponent implements OnInit {

  constructor(private route: ActivatedRoute, private apollo: Apollo) { }
  code: any;
  data: any[] = [];
  result: any;
  role = localStorage.getItem('role');
  userId = localStorage.getItem('userId');
  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.code = params['input'];
    })
    // this.apollo
    //   .watchQuery({
    //     query: getRecentPublishedTopics,
    //     variables: {
    //       facultyId: localStorage.getItem("facultyId")
    //     }
    //   })
    //   .valueChanges.subscribe((result: any) => {
    //     this.data = result.data.getRecentPublishedTopics
    //     this.result = this.data.find(element => element.id === this.code)
    //     console.log(this.data)
    //   });
    if (this.role != 'MANAGER') {
      this.apollo.watchQuery({
        query: searchTopicsByNameAndCode,
        variables: {
          searchString: this.code
        }
      })
        .valueChanges.subscribe((result: any) => {
          const data = result.data.searchTopicsByNameAndCode
          this.result = data;
          console.log(this.result)
        });
    }
    if (this.role == 'MANAGER') {
      this.apollo.watchQuery({
        query: searchAllPublishedTopics,
        variables: {
          searchString: this.code
        }
      })
        .valueChanges.subscribe((result: any) => {
          const data = result.data.searchAllPublishedTopics
          this.result = data;
          console.log(this.result)
        });
    }
    if (this.role == 'USER') {
      this.apollo.watchQuery({
        query: searchAllPublishedTopics,
        variables: {
          searchString: this.code
        }
      })
        .valueChanges.subscribe((result: any) => {
          const data = result.data.searchAllPublishedTopics
          this.result = data;
          console.log(this.result)
        });
    }

  }



  getDate(value: string) {
    var date = new Date(value);
    return date
  }
  search(topicCode: string) {
    if (this.role == 'STUDENT') {
      // window.open("http://localhost:4200/index/upload?id=" + topicCode, "_self");
      window.open("http://localhost:4200/index/upload?id=" + topicCode, "_self");
    }
    if (this.role == 'COORDINATOR') {
      window.open("http://localhost:4200/index/topicList?id=" + topicCode, "_self");
    }
    if (this.role == 'MANAGER') {
      window.open("http://localhost:4200/index/topicList?id=" + topicCode, "_self");
    }
    if (this.role == 'USER') {
      window.open("http://localhost:4200/index/topic?id=" + topicCode, "_self");
    }
  }
}
