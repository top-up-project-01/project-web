import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Apollo } from 'apollo-angular';
import { getApprovedSubmissionOfPublishedTopic } from 'src/app/graphql/submission';
import { searchAllPublishedTopics } from 'src/app/graphql/topic';

@Component({
  selector: 'app-submissionDetail',
  templateUrl: './submissionDetail.component.html',
  styleUrls: ['./submissionDetail.component.scss']
})
export class SubmissionDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private apollo: Apollo) { }
  isSecondDeadline?: boolean;
  topicCode?: string;
  topicId?: string;
  topicName?: string;
  subId: any;
  firstDeadline: any;
  secondDeadline: any;
  publishDate: any;
  today = new Date();
  description: any;
  status: any;


  // Thông tin user
  userId = localStorage.getItem('userId');
  role = localStorage.getItem('role');
  fullName = localStorage.getItem('fullName');
  facultyId = localStorage.getItem('facultyId');
  getSubid: any;
  getData1: any;
  getData2: any[] = [];
  dataTopic: any[] = [];
  dataSubmisstion: any[] = [];
  listSubmisstion: any;
  result: any;
  contentUrls: any;
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.topicCode = params.id
      this.getSubid = params.subId
    })
    this.apollo.watchQuery({
      query: searchAllPublishedTopics,
      variables: {
        searchString: this.topicCode,
      }
    })
      .valueChanges.subscribe((result: any) => {
        this.dataTopic = result.data.searchAllPublishedTopics
        this.result = this.dataTopic.find(element => element.topicCode === this.topicCode)
        this.firstDeadline = this.result['firstDeadline']
        this.secondDeadline = this.result['secondDeadline']
        this.publishDate = this.result['publishDate']
        this.description = this.result['description']
        this.topicId = this.result['id']
        this.topicName = this.result['name']
        console.log("topicName : " + this.topicName)
        console.log("user ID: " + this.userId)
        console.log("topic ID: " + this.topicId)
        console.log("firstDeadline: " + this.firstDeadline)
        console.log("secondDeadline: " + this.secondDeadline)
        console.log("publishDate: " + this.publishDate)

        this.apollo.watchQuery({
          query: getApprovedSubmissionOfPublishedTopic,
          variables: {
            facultyId: this.facultyId,
            topicId: this.topicId,
          }
        })
          .valueChanges.subscribe((result: any) => {
            this.dataSubmisstion = result.data.getApprovedSubmissionOfPublishedTopic;
            this.listSubmisstion = this.dataSubmisstion.find(element => element.id === this.getSubid)
            this.contentUrls = this.listSubmisstion.contentUrls
            console.log(this.listSubmisstion.student.user);
          })
      });
  }
  getDate(value: string) {
    var date = new Date(value);
    return date
  }
}
