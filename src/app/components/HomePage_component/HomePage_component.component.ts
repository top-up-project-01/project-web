import { Component, ElementRef, OnInit, ViewChild, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Apollo } from 'apollo-angular';
import { getRecentPublishedTopics } from 'src/app/graphql/topic';
import { allFaculties } from '../../graphql/faculty';
import ApexCharts from 'apexcharts';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-HomePage_component',
  templateUrl: './HomePage_component.component.html',
  styleUrls: ['./HomePage_component.component.css']
})
export class HomePage_componentComponent implements OnInit {

  constructor(private apollo: Apollo, private route: ActivatedRoute,) { }
  token: any;
  topic_code: any;
  inputCode: any;
  faculties: any;
  recentTopic: any;
  index = 0;
  page: any;
  card1 = 0;
  card2 = 1;
  i: any;
  topic: any;
  popupChatBox = false;

  public showArrow = true;

  imgSrc: any[] = [
    '../../../assets/img/imageConten1.jpg',
    '../../../assets/img/imageConten2.jpg',
  ];

  increaseIndex() {
    this.index += 1;
  }
  decreaseIndex() {
    this.index -= 1;
  }
  openForm() {
    this.popupChatBox = true;

  }

  closeForm() {
    this.popupChatBox = false;
  }
  ngOnInit(): void {
    var options = {
      chart: {
        type: 'line',
      },
      series: [{
        name: 'Submissions',
        data: [5, 15, 35, 20, 25, 30, 5, 30, 45]
      }],
      xaxis: {
        categories: ['Date 1', 'Date 2', 'Date 3', 'Date 4', 'Date 5', 'Date 6', 'Date 7']
      }
    }

    var chart = new ApexCharts(document.querySelector("#chart"), options);

    chart.render();
    this.token = localStorage.getItem('token');

    this.apollo
      .watchQuery({
        query: getRecentPublishedTopics,
        variables: {
          facultyId: localStorage.getItem("facultyId")
        }
      })
      .valueChanges.subscribe((result: any) => {
        this.recentTopic = result.data.getRecentPublishedTopics
        console.log(this.recentTopic)
      });
    this.route.queryParams.subscribe(params => {
      this.page = params.page;
      this.i = this.page
    })
  }

  openTopic(topic_code: string) {

    window.open("http://localhost:4200/index/topic?id=" + topic_code, "_self");

  }
  getDate(value: string) {
    var date = new Date(value);
    return date
  }
  search() {
    this.topic_code = this.inputCode
    window.open("http://localhost:4200/index/homePage/search?input=" + this.topic_code, "_self");
  }
  imageObject: Array<object> = [{
    image: '../../../assets/img/imageConten1.jpg',
    thumbImage: '../../../assets/img/imageConten1.jpg',

  }, {
    image: '../../../assets/img/imageConten2.jpg',
    thumbImage: '../../../assets/img/imageConten2.jpg',
  },
  {
    image: '../../../assets/img/imageConten3.jpg',
    thumbImage: '../../../assets/img/imageConten3.jpg',
  },

  ];

  ngOnChanges(): void {
    this.showArrow = false;
  }
}
