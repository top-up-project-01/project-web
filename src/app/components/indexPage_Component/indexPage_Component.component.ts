import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { allFaculties } from '../../graphql/faculty';
import ApexCharts from 'apexcharts'
@Component({
  selector: 'app-indexPage_Component',
  templateUrl: './indexPage_Component.component.html',
  styleUrls: ['./indexPage_Component.component.css']
})
export class IndexPage_ComponentComponent implements OnInit {

  constructor(private apollo: Apollo) { }
  _opened: boolean = false;
  _closeOnClickOutside: boolean = true;
  _closeOnClickBackdrop: boolean = true;
  _showBackdrop: boolean = true;
  _toggleSidebar() {
    this._opened = !this._opened;
  }

  token: any;
  role: any;
  facultyId: any;
  userId: any;
  email: any;
  fullName: any;
  ngOnInit(): void {
    var options = {
      chart: {
        type: 'line',
      },
      series: [{
        name: 'Submissions',
        data: [5, 15, 35, 20, 25, 30, 5, 30, 45]
      }],
      xaxis: {
        categories: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
      }
    }

    var chart = new ApexCharts(document.querySelector("#chart"), options);

    this.token = localStorage.getItem('token');
    console.log('=> token')
    console.log(this.token)
    this.role = localStorage.getItem('role');
    console.log('=> role')
    console.log(this.role)
    this.facultyId = localStorage.getItem('facultyId');
    console.log('=> facultyId')
    console.log(this.facultyId)
    this.userId = localStorage.getItem('userId');
    console.log('=> userId')
    console.log(this.userId)
    this.email = localStorage.getItem('email');
    console.log('=> email')
    console.log(this.email)
    this.fullName = localStorage.getItem('fullName');
    console.log('=> fullName')
    console.log(this.fullName)
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('fullName');
    localStorage.removeItem('userId');
    localStorage.removeItem('facultyId');
    window.open("/login", "_self")
  }
}