import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Apollo } from 'apollo-angular';
import { searchTopicsByNameAndCode } from 'src/app/graphql/topic';
import { allFaculties } from '../../graphql/faculty';
import { addCommentToASubmission, getCommentsOfSubmission } from '../../graphql/comment';
import { uploadFiles } from '../../graphql/student';
import { getApprovedSubmission, getSubmissionOfCurrentStudent, getSubmissionsOfTopic, updateSubmissionStatus, } from '../../graphql/submission'


@Component({
  selector: 'app-upload_page',
  templateUrl: './upload_page.component.html',
  styleUrls: ['./upload_page.component.scss']
})
export class Upload_pageComponent implements OnInit {
  result: any;
  selectedValue = null;
  selectedFiles: any;
  message = '';
  progressInfos: any[] = [];
  isSecondDeadline?: boolean;
  topicCode?: string;
  topicId?: string;
  topicName?: string;
  subId: any;
  firstDeadline: any;
  secondDeadline: any;
  today = new Date();
  check?: boolean;
  submit: any;
  commentList: any;
  commentAuthor: any;
  userId = localStorage.getItem('userId');
  role = localStorage.getItem('role');
  fullName = localStorage.getItem('fullName');
  facultyId = localStorage.getItem('facultyId');
  inputComment: any;
  dataTopic: any[] = [];
  dataComment: any[] = [];
  status: any;
  getSubid: any;
  dataSubmisstion: any[] = [];
  listSubmisstion: any;
  contentUrls: any;
  constructor(private route: ActivatedRoute, private apollo: Apollo) { }
  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.topicCode = params.id
      this.getSubid = params.subId
    })

    this.apollo.watchQuery({
      query: searchTopicsByNameAndCode,
      variables: {
        searchString: this.topicCode
      }
    })

      // Lấy TopicId
      .valueChanges.subscribe((result: any) => {
        this.dataTopic = result.data.searchTopicsByNameAndCode
        this.result = this.dataTopic.find(element => element.topicCode === this.topicCode)
        this.firstDeadline = this.result['firstDeadline']
        this.secondDeadline = this.result['secondDeadline']
        this.topicId = this.result['id']
        this.topicName = this.result['name']
        console.log("GetSubId= " + this.getSubid)
        console.log("GetUserId= " + this.userId)
        console.log(this.topicId)

        if (this.role == 'STUDENT') {
          // Lấy SubId
          this.apollo.watchQuery({
            query: getSubmissionOfCurrentStudent,
            variables: {
              userId: this.userId,
              topicId: this.topicId,
            }
          })
            .valueChanges.subscribe((result: any) => {
              var data = result.data.getSubmissionOfCurrentStudent;
              this.submit = data;
              this.subId = data['id']
              this.status = data['status']
              this.contentUrls = data.contentUrls
              console.log("status : " + this.status)
              console.log("topicName : " + this.topicName)
              console.log("sub ID: " + this.subId)
              console.log("user ID: " + this.userId)
              console.log("topic ID: " + this.topicId)
              console.log("firstDeadline: " + this.firstDeadline)
              console.log("secondDeadline: " + this.secondDeadline)
              // Lấy list comment
              this.apollo.watchQuery({
                query: getCommentsOfSubmission,
                variables: {
                  subId: this.subId
                }
              })
                .valueChanges.subscribe((result: any) => {
                  var data = result.data.getCommentsOfSubmission;
                  this.commentList = data
                  this.dataComment = result.data.getCommentsOfSubmission;
                  this.commentAuthor = this.dataComment.map(({ user }) => user)
                  console.log(this.commentAuthor)
                })
            })
        }
        if (this.role == 'COORDINATOR') {
          console.log("hello")
          this.apollo.watchQuery({
            query: getSubmissionsOfTopic,
            variables: {
              facultyId: this.facultyId,
              topicId: this.topicId,
            }
          })
            .valueChanges.subscribe((result: any) => {
              this.dataSubmisstion = result.data.getSubmissionsOfTopic;
              console.log(this.dataSubmisstion);
              this.listSubmisstion = this.dataSubmisstion.find(element => element.id === this.getSubid)
              this.contentUrls = this.listSubmisstion.contentUrls;
              console.log(this.contentUrls);

            })
          this.apollo.watchQuery({
            query: getCommentsOfSubmission,
            variables: {
              subId: this.getSubid
            }
          })
            .valueChanges.subscribe((result: any) => {
              var data = result.data.getCommentsOfSubmission;
              this.commentList = data
              this.dataComment = result.data.getCommentsOfSubmission;
              this.commentAuthor = this.dataComment.map(({ user }) => user)
              console.log(this.commentAuthor)
            })
        }




      });




    // note lấy subId
  }
  saveSatus() {
    console.log(this.selectedValue)
  }
  getDate(value: string) {
    var date = new Date(value);
    return date
  }
  selectFiles(event: any) {
    this.progressInfos = [];
    this.selectedFiles = event.target.files;
    for (let file of this.selectedFiles) {
      this.progressInfos.push(file.name);
    }
  }

  uploadFilesDeadline1() {
    console.log(this.selectedFiles)
    if (this.selectedFiles) {
      this.apollo.mutate({
        mutation: uploadFiles,
        variables: {
          files: {
            contents: this.selectedFiles
          },
          userId: this.userId,
          topicId: this.topicId,
          isSecondDeadline: false,
        },
        context: {
          useMultipart: true
        }
      }).subscribe(async ({ data }) => {
        const res = data as any
        console.log(res)
        window.alert("Upload Successfully");
      })
    }
  }

  uploadFilesDeadline2() {
    console.log(this.selectedFiles)
    if (this.selectedFiles) {
      this.apollo.mutate({
        mutation: uploadFiles,
        variables: {
          files: {
            contents: this.selectedFiles
          },
          userId: this.userId,
          topicId: this.topicId,
          isSecondDeadline: true,
        },
        context: {
          useMultipart: true
        }
      }).subscribe(async ({ data }) => {
        const res = data as any
        console.log(res)
        window.alert("Upload Successfully");
      })
    }
  }

  updateSatus() {
    this.apollo.mutate({
      mutation: updateSubmissionStatus,
      variables: {
        subId: this.getSubid,
        status: this.selectedValue,
      }
    }).subscribe(async ({ data }) => {
      const res = data as any
      console.log(res)
      window.alert("Upload Successfully");
    })
  }

  comment() {
    if (this.role == 'STUDENT') {
      this.apollo.mutate({
        mutation: addCommentToASubmission,
        variables: {
          content: this.inputComment,
          userId: this.userId,
          subId: this.subId,
        }
      }).subscribe(async ({ data }) => {
        const res = data as any
        console.log(res)
        location.reload();
      })
      console.log("Input comment: " + this.inputComment)
    }
    if (this.role == 'COORDINATOR') {
      this.apollo.mutate({
        mutation: addCommentToASubmission,
        variables: {
          content: this.inputComment,
          userId: this.userId,
          subId: this.getSubid
        }
      }).subscribe(async ({ data }) => {
        const res = data as any
        console.log(res)
        location.reload();
      })
      console.log("Input comment: " + this.inputComment)
    }
  }





  // data: any[] = [];
  // users: any[] = [];
  // users2: any[] = [];
  // users3: any;
  // ngOnInit(): void {
  //   this.apollo
  //     .watchQuery({
  //       query: allFaculties
  //     })
  //     .valueChanges.subscribe((result: any) => {
  //       this.data = result.data.allFaculties
  //       console.log(this.data)
  //       this.users = this.data.map(({ users }) => users);
  //       console.log('All user')
  //       console.log(this.users)
  //       this.users2 = this.users.find(element => element)
  //       console.log('Get users item:')
  //       console.log(this.users2)
  //       console.log('User name Tony:')
  //       console.log(this.users2.find(element => element.fullName === 'Tony Nguyen'))
  //       this.users3 = this.users2.find(element => element.fullName === 'Tony Nguyen')
  //       console.log(" User name: " + this.users3.fullName)
  //     });
  // }

  // getApporeve() {
  //   this.apollo.watchQuery({
  //     query: getApprovedSubmission,
  //     variables: {
  //       facultyId: "52a0e31e-977e-4b29-a79a-8d82f42c897a",
  //       topicId: "38305011-9ce7-48a1-afc6-c9fae761b38d",
  //     }
  //   })
  //     .valueChanges.subscribe((result: any) => {
  //       var data = result.data.getApprovedSubmission;
  //       console.log(data);
  //     })
  // }

}