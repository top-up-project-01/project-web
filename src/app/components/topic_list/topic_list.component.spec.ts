/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Topic_listComponent } from './topic_list.component';

describe('Topic_listComponent', () => {
  let component: Topic_listComponent;
  let fixture: ComponentFixture<Topic_listComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Topic_listComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Topic_listComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
