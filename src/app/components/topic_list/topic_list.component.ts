import { Component, OnInit } from '@angular/core';
import { topicData } from "../../models/topic";
import { getApprovedSubmission, getSubmissionOfCurrentStudent, getSubmissionsOfTopic } from '../../graphql/submission'
import { Apollo } from 'apollo-angular';
import { ActivatedRoute } from '@angular/router';
import { searchTopicsByNameAndCode, searchAllPublishedTopics } from 'src/app/graphql/topic';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Component({
  selector: 'app-topic_list',
  templateUrl: './topic_list.component.html',
  styleUrls: ['./topic_list.component.scss']
})
export class Topic_listComponent implements OnInit {
  topicData: any[] = topicData;
  constructor(private route: ActivatedRoute, private apollo: Apollo, private readonly http: HttpClient) { }

  // Thông tin Topic
  isSecondDeadline?: boolean;
  topicCode?: string;
  topicId?: string;
  topicName?: string;
  subId: any;
  firstDeadline: any;
  secondDeadline: any;
  publishDate: any;
  today = new Date();
  description: any;
  status: any;


  // Thông tin user
  userId = localStorage.getItem('userId');
  role = localStorage.getItem('role');
  fullName = localStorage.getItem('fullName');
  facultyId = localStorage.getItem('facultyId');

  getData1: any;
  getData2: any[] = [];
  dataTopic: any[] = [];
  dataSubmisstion: any[] = [];
  listSubmisstion: any;
  result: any;

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.topicCode = params.id
    })
    if (this.role != 'MANAGER') {
      this.apollo.watchQuery({
        query: searchTopicsByNameAndCode,
        variables: {
          searchString: this.topicCode
        }
      })

        // Lấy TopicId
        .valueChanges.subscribe((result: any) => {
          this.dataTopic = result.data.searchTopicsByNameAndCode
          this.result = this.dataTopic.find(element => element.topicCode === this.topicCode)
          this.firstDeadline = this.result['firstDeadline']
          this.secondDeadline = this.result['secondDeadline']
          this.publishDate = this.result['publishDate']
          this.description = this.result['description']
          this.topicId = this.result['id']
          this.topicName = this.result['name']
          console.log(this.result)
          console.log("topicName : " + this.topicName)
          console.log("user ID: " + this.userId)
          console.log("topic ID: " + this.topicId)
          console.log("firstDeadline: " + this.firstDeadline)
          console.log("secondDeadline: " + this.secondDeadline)
          console.log("publishDate: " + this.publishDate)
          if (this.role === 'MANAGER') {
            this.apollo.watchQuery({
              query: getApprovedSubmission,
              variables: {
                facultyId: this.facultyId,
                topicId: this.topicId,
              }
            })
              .valueChanges.subscribe((result: any) => {
                this.dataSubmisstion = result.data.getApprovedSubmission;
                console.log(this.dataSubmisstion);
                this.listSubmisstion = this.dataSubmisstion.map(({ student }) => student);
              })
          }
          if (this.role == 'COORDINATOR') {
            this.apollo.watchQuery({
              query: getSubmissionsOfTopic,
              variables: {
                facultyId: this.facultyId,
                topicId: this.topicId,
              }
            })
              .valueChanges.subscribe((result: any) => {
                this.dataSubmisstion = result.data.getSubmissionsOfTopic;
                console.log(this.dataSubmisstion);
                this.listSubmisstion = this.dataSubmisstion.map(({ student }) => student);
              })
          }
        });
    }
    if (this.role == 'MANAGER') {
      this.apollo.watchQuery({
        query: searchAllPublishedTopics,
        variables: {
          searchString: this.topicCode
        }
      })

        // Lấy TopicId
        .valueChanges.subscribe((result: any) => {
          this.dataTopic = result.data.searchAllPublishedTopics
          this.result = this.dataTopic.find(element => element.topicCode === this.topicCode)
          this.firstDeadline = this.result['firstDeadline']
          this.secondDeadline = this.result['secondDeadline']
          this.publishDate = this.result['publishDate']
          this.description = this.result['description']
          this.topicId = this.result['id']
          this.topicName = this.result['name']
          console.log(this.result)
          console.log("topicName : " + this.topicName)
          console.log("user ID: " + this.userId)
          console.log("topic ID: " + this.topicId)
          console.log("firstDeadline: " + this.firstDeadline)
          console.log("secondDeadline: " + this.secondDeadline)
          console.log("publishDate: " + this.publishDate)
          if (this.role === 'MANAGER') {
            this.apollo.watchQuery({
              query: getApprovedSubmission,
              variables: {
                facultyId: this.facultyId,
                topicId: this.topicId,
              }
            })
              .valueChanges.subscribe((result: any) => {
                this.dataSubmisstion = result.data.getApprovedSubmission;
                console.log(this.dataSubmisstion);
                this.listSubmisstion = this.dataSubmisstion.map(({ student }) => student);
              })
          }
          if (this.role == 'COORDINATOR') {
            this.apollo.watchQuery({
              query: getSubmissionsOfTopic,
              variables: {
                facultyId: this.facultyId,
                topicId: this.topicId,
              }
            })
              .valueChanges.subscribe((result: any) => {
                this.dataSubmisstion = result.data.getSubmissionsOfTopic;
                console.log(this.dataSubmisstion);
                this.listSubmisstion = this.dataSubmisstion.map(({ student }) => student);
              })
          }
        });

    }




  }

  getDate(value: string) {
    var date = new Date(value);
    return date
  }

  saveZipForManager() {
    const token = localStorage.getItem('token');
    const params = new HttpParams().append('topicId', String(this.topicId)).append('facultyId', String(this.facultyId));
    const headers = new HttpHeaders().append('Authorization', `Bearer ${token}`);

    this.http.get<any>('http://localhost:3000/file', { headers, params, responseType: 'blob' as 'json' })
      .subscribe((data) => {
        var downloadURL = window.URL.createObjectURL(data);
        var link = document.createElement('a');
        link.href = downloadURL;
        //add topic's name/id here
        link.download = "topic.zip";
        link.click();
      });
  }
  openSubmission(subId: string) {
    window.open("http://localhost:4200/index/submissionDetail?id=" + this.topicCode + "&subId=" + subId, "_self");
  }
  openUpload(subId: string) {
    window.open("http://localhost:4200/index/upload?id=" + this.topicCode + "&subId=" + subId, "_self");
  }
}
