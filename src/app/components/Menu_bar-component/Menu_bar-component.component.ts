import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-Menu_bar-component',
  templateUrl: './Menu_bar-component.component.html',
  styleUrls: ['./Menu_bar-component.component.css']
})
export class Menu_barComponentComponent implements OnInit {
  token: any;
  role: any;
  fullName: any;
  firstName: any;
  constructor() { }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.role = localStorage.getItem('role');
    this.fullName = localStorage.getItem('fullName');
    var details = []
    details = this.fullName.split(' ');
    this.firstName = details[0]
  }


  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('fullName');
    localStorage.removeItem('userId');
    localStorage.removeItem('facultyId');
    window.open("/login", "_self")
  }
}
