/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { RecenTopicComponent } from './recenTopic.component';

describe('RecenTopicComponent', () => {
  let component: RecenTopicComponent;
  let fixture: ComponentFixture<RecenTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecenTopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecenTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
