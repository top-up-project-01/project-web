import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getRecentPublishedTopics } from 'src/app/graphql/topic';
import { Apollo } from 'apollo-angular';
@Component({
  selector: 'app-recenTopic',
  templateUrl: './recenTopic.component.html',
  styleUrls: ['./recenTopic.component.scss']
})
export class RecenTopicComponent implements OnInit {

  constructor(private route: ActivatedRoute, private apollo: Apollo) { }
  page: any;
  card1 = 0;
  card2 = 1;
  i: any;
  topic: any;
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.page = params.page;
      this.i = this.page
    })
    this.apollo
      .watchQuery({
        query: getRecentPublishedTopics,
        variables: {
          facultyId: localStorage.getItem("facultyId")
        }
      })
      .valueChanges.subscribe((result: any) => {
        this.topic = result.data.getRecentPublishedTopics
        console.log(this.topic)
      });
  }
  getDate(value: string) {
    var date = new Date(value);
    return date
  }
  openTopic(topic_code: string) {
    window.open("http://localhost:4200/index/topic?id=" + topic_code, "_self");
  }
}
