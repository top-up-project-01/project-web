import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { DeviceDetectorService } from 'ngx-device-detector';
import { login } from '../../graphql/user';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-Login-component',
  templateUrl: './Login-component.component.html',
  styleUrls: ['./Login-component.component.css']
})

export class LoginComponentComponent implements OnInit {
  email = "";
  password = "";



  constructor(private apollo: Apollo, private deviceService: DeviceDetectorService, private _snackBar: MatSnackBar) { this.epicFunction() }

  ngOnInit() {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem('role');
    const fullName = localStorage.getItem('fullName');
    const facultyId = localStorage.getItem('facultyId');
    const userId = localStorage.getItem('userId');

    console.log({ token })
    console.log({ role })
    console.log({ fullName })
    console.log({ facultyId })
    console.log({ userId })


    if (token != null) {
      window.open("/index/homePage", "_self")
    }
  }
  public pcView = false;
  public tabletView = false;
  public mobileView = false;
  message = "caption"

  signIn() {

    this.apollo.mutate({
      mutation: login,
      variables: {
        user: {
          email: this.email,
          password: this.password
        }
      }
    }).subscribe(async ({ data }) => {
      const res = data as any
      const token = res.login.token
      const user = res.login.user


      localStorage.setItem("token", token);
      localStorage.setItem("role", user.role)
      localStorage.setItem("userId", user.id)
      localStorage.setItem("fullName", user.fullName)
      localStorage.setItem("facultyId", user.faculty.id)
      localStorage.setItem("email", user.email)
      const token2 = localStorage.getItem('token');
      const userId = localStorage.getItem('userId');
      const role = localStorage.getItem('role');
      const facultyId = localStorage.getItem('facultyId');
      const email = localStorage.getItem('email');
      console.log('=> token')
      console.log(token2)
      console.log('=> user')
      console.log(user)
      console.log('=> role')
      console.log(role)
      console.log('=> facultyId')
      console.log(facultyId)
      console.log('=> userId')
      console.log(userId)
      console.log('=> email')
      console.log(email)
      setTimeout(() => {
        window.open("/index/homePage/", "_self")
      },
        // 10000
      )

    },
      (error) => {
        console.log(error.message)
        window.alert(error.message);
      });
  }

  epicFunction() {
    const isMobile = this.deviceService.isMobile();
    const isTablet = this.deviceService.isTablet();
    const isDesktopDevice = this.deviceService.isDesktop();
    this.pcView = isDesktopDevice;
    this.tabletView = isTablet;
    this.mobileView = isMobile;
  }



}


