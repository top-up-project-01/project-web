import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { uploadFiles } from '../../graphql/student';

@Component({
  selector: 'app-StudentPage_Component',
  templateUrl: './StudentPage_Component.component.html',
  styleUrls: ['./StudentPage_Component.component.scss']
})
export class StudentPage_ComponentComponent implements OnInit {
  selectedFiles: FileList | undefined;
  progressInfos = [];
  message = '';

  constructor(private apollo: Apollo) {

  }

  ngOnInit() {
  }

  selectFiles(event: any) {
    this.progressInfos = [];
    this.selectedFiles = event.target.files;
  }

  uploadFiles() {
    console.log(this.selectedFiles)
    if (this.selectedFiles) {
      this.apollo.mutate({
        mutation: uploadFiles,
        variables: {
          files: {
            contents: this.selectedFiles
          }
        },
        context: {
          useMultipart: true
        }
      }).subscribe(async ({ data }) => {
        const res = data as any
        console.log(res)
      })
    }

  }
}
