import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { allFaculties } from '../../graphql/faculty';

@Component({
  selector: 'app-footer-component',
  templateUrl: './footer-component.component.html',
  styleUrls: ['./footer-component.component.css']
})
export class FooterComponentComponent implements OnInit {

  constructor(private apollo: Apollo) { }

  ngOnInit(): void {
  }

}
