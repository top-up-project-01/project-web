import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactUsComponent } from './components/contactUs/contactUs.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomePage_componentComponent } from './components/HomePage_component/HomePage_component.component';
import { IndexPage_ComponentComponent } from './components/indexPage_Component/indexPage_Component.component';
import { LoginComponentComponent } from './components/Login-component/Login-component.component';
import { RecenTopicComponent } from './components/recenTopic/recenTopic.component';
import { SearchComponentComponent } from './components/search-component/search-component.component';
import { SearchDashboardPageComponent } from './components/searchDashboardPage/searchDashboardPage.component';
import { SubmissionDetailComponent } from './components/submissionDetail/submissionDetail.component';
import { Topic_componentComponent } from './components/topic_component/topic_component.component';
import { Topic_listComponent } from './components/topic_list/topic_list.component';
import { Upload_pageComponent } from './components/upload_page/upload_page.component';


export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'index', redirectTo: 'index/homePage', pathMatch: 'full' },

  { path: 'login', component: LoginComponentComponent },

  {
    path: 'index', component: IndexPage_ComponentComponent,

    children: [
      {
        path: 'homePage', component: HomePage_componentComponent,
      },
      { path: 'topicList', component: Topic_listComponent },
      { path: 'contactUs', component: ContactUsComponent },
      { path: 'upload', component: Upload_pageComponent },
      { path: 'submissionDetail', component: SubmissionDetailComponent },
      { path: 'topic', component: Topic_componentComponent },

      {
        path: 'dashboard', component: DashboardComponent,
        children: [
          { path: 'search', component: SearchDashboardPageComponent },
        ]
      },
      { path: 'homePage/search', component: SearchComponentComponent },
    ]
  },
]; @NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
