export default class Topic {
    description: string | undefined;
    id: string | undefined;
    name: string | undefined;
    publishDate: string | undefined;
    secondDeadline: string | undefined;
    firstDeadline: string | undefined;

}
export const topicData = [
    {
        title: "submisson1",
        author: "Gabriel",
        subDate: "13-01-2021",
        url: "example"
    },
    {
        title: "submisson2",
        author: "Fatima",
        subDate: "15-01-2021",
        url: "example"
    },
    {
        title: "submisson3",
        author: "Eric",
        subDate: "22-01-2021",
        url: "example"
    },
    {
        title: "submisson4",
        author: "Wendell",
        subDate: "02-01-2021",
        url: "example"
    },
    {
        title: "submisson5",
        author: "Dana",
        subDate: "21-01-2021",
        url: "example"
    }
]