import { gql } from "apollo-angular";

export const getSubmissionOfCurrentStudent = gql`
  query getSubmissionOfCurrentStudent($userId: ID!, $topicId: ID!) {
      getSubmissionOfCurrentStudent(userId:$userId, topicId: $topicId) {
          id
          contentUrls
          status
      }
  }
`
export const updateSubmissionStatus = gql`
mutation updateSubmissionStatus($status: Status, $subId: ID!) {
    updateSubmissionStatus(status: $status, subId: $subId) {
     id
     status
    }
}
`
export const getApprovedSubmission = gql`
  query getApprovedSubmission($facultyId: ID!, $topicId: ID!) {
      getApprovedSubmission(facultyId:$facultyId, topicId: $topicId) {
          id
          createdAt
          status
          contentUrls
          student{
            user{
              email
              fullName
            }
          }
      }
  }
`
export const getSubmissionsOfTopic = gql`
  query getSubmissionsOfTopic($facultyId: ID!, $topicId: ID!) {
      getSubmissionsOfTopic(facultyId:$facultyId, topicId: $topicId) {
          id
          createdAt
          status
          contentUrls
          student{
            user{
              email
              fullName
            }
          }
      }
  }
`
export const getApprovedSubmissionOfPublishedTopic = gql`
  query getApprovedSubmissionOfPublishedTopic($facultyId: ID!, $topicId: ID!) {
      getApprovedSubmissionOfPublishedTopic(facultyId:$facultyId, topicId: $topicId) {
          id
          createdAt
          status
          contentUrls
          student{
            user{
              email
              fullName
            }
          }
      }
  }
`