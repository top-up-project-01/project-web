import { gql } from "apollo-angular";

export const getRecentPublishedTopics = gql`
   query getRecentPublishedTopics($facultyId: ID) {
       getRecentPublishedTopics(facultyId: $facultyId) {
           id
           name
           topicCode
           description
           secondDeadline
           publishDate
           firstDeadline
       }
  }
`

export const searchTopicsByNameAndCode = gql`
    query searchTopicsByNameAndCode($searchString: String) {
        searchTopicsByNameAndCode(searchString: $searchString) {
            id
            name
            description
            publishDate
            firstDeadline
            topicCode
            secondDeadline
        }
    }
`
export const searchAllPublishedTopics = gql`
    query searchAllPublishedTopics($searchString: String) {
        searchAllPublishedTopics(searchString: $searchString) {
            id
            name
            description
            publishDate
            firstDeadline
            topicCode
            secondDeadline
        }
    }
`