import { gql } from "apollo-angular";

export const login = gql`
    mutation login($user: UserPayloadInput) {
        login(user: $user) {
            token
            user {
                id
                role
                email
                fullName
                faculty {
                    id
                }
            }
        }
    }
`

