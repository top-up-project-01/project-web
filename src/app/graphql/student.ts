import { gql } from "apollo-angular";

export const uploadFiles = gql`
    mutation uploadFiles($files: StudentFiles, $userId: ID!, $topicId: ID!, $isSecondDeadline: Boolean) {
        studentUploadFiles(files: $files, userId: $userId, topicId: $topicId, isSecondDeadline: $isSecondDeadline) {
            message
            status
        }
    }
`
