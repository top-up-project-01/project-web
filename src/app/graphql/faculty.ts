import { gql } from "apollo-angular";

export const allFaculties = gql`
    query {
        allFaculties {
            id
            description
            name
            users {
                id
                fullName
                email
                role
            }
        }
    }
`

