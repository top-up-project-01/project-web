import { gql } from "apollo-angular";

export const addCommentToASubmission = gql`
mutation addCommentToASubmission($userId: ID!, $content: String, $subId: ID!) {
    addCommentToASubmission(userId: $userId, content: $content, subId: $subId) {
        content
        id
        createdAt
        user {
            role
            email
            fullName
        }
    }
}
`
export const getCommentsOfSubmission = gql`
  query getCommentsOfSubmission($subId: ID!) {
    getCommentsOfSubmission(subId: $subId) {
      id
      content
      createdAt
      user {
        id
        role
        email
        fullName
      }
    }
  }
`
